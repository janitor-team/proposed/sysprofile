# Finding and maybe deleting core files in the users's $HOME to 
# reclaim lost disk space


delcores()
{
 set -x
 for file in `find $HOME -name core -type f -print`
 do
  if [ `/usr/bin/file $file | /bin/grep -c "core file"` != 0 ]; then 
        /usr/bin/gdb -batch -q -c $file
        /bin/ls -l $file
        #/bin/rm -f $file
  else
        echo "File named \"core\" is not a core file"
  fi
 done
}

if [ -d /var/tmp/syslogout ]; then
        delcores >> /var/tmp/syslogout/$USER 2>&1 &
else
        delcores &> /dev/null &
fi
