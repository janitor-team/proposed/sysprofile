# Remove the leftovers of $USER in /tmp

# Uncomment for debugging:
#set -x

for file in `find /tmp -user $USER -maxdepth 1 -name "*"`
do
   if [ -d /var/tmp/syslogout ]; then
        ls -l $file >> /var/tmp/syslogout/$USER
   else
        ls -l $file #&> /dev/null
   fi
done
