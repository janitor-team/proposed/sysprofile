# Uncomment for debugging:
#set -x

BACKUPSCRIPT=/usr/local/sbin/backupscript

for dir in $HOME/whatever
 do
   if [ -d /var/tmp/syslogout ]; then
      nohup $BACKUPSCRIPT $dir >> /var/tmp/syslogout/$USER 2>&1 &
   else
      nohup $BACKUPSCRIPT $dir &> /dev/null &
 done
fi
