# WordPerfect sometimes refuses to quit:

# Uncomment for debugging:
#set -x

for prog in wpexc
do
 if [ "`ps --user $USER | grep -c $prog`" != 0 ]; then 
     if [ -d /var/tmp/syslogout ]; then
         killall -v -9 $prog >> /var/tmp/syslogout/$USER 2>&1
     else
         killall -v -9 $prog &> /dev/null
     fi
 fi
done

# Remove the leftovers of a WordPerfect session:

if [ -d /tmp/wpc-$HOSTNAME ]; then
  if [ -d /var/tmp/syslogout ]; then
        rm -rf `find /tmp -user $USER -maxdepth 1 -name "wpc-$HOSTNAME"`\
         >> /var/tmp/syslogout/$USER 2>&1
  else
        rm -rf `find /tmp -user $USER -maxdepth 1 -name "wpc-$HOSTNAME"`\
         &> /dev/null
  fi
fi
