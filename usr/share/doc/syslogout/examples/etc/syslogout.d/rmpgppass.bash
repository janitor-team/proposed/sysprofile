# Remove any PGPPASS entries from shell history:

# Uncomment for debugging:
#set -x

sed '/^export PGPPASS/d'\
  ~/.bash_history > ~/.bash_history.tmp &&\
  mv ~/.bash_history.tmp ~/.bash_history &&\
  chmod 640 ~/.bash_history
