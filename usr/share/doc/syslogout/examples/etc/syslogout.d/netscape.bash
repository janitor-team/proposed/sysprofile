# These programs sometimes crash and eat 
# up system resources even after logout.

# Uncomment for debugging:
#set -x

for prog in navigator communicator
do
  if [ "`ps --user $USER | grep -c $prog`" != 0 ]; then 
    if [ -d /var/tmp/syslogout ]; then
         killall -v -9 $prog-smotif.real\
          >> /var/tmp/syslogout/$USER 2>&1
    else
        killall -v -9 $prog-smotif.real\
         &> /dev/null
    fi
 fi
done
