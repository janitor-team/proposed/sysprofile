# This is probably only useful within pure shell environments
# and will only create unexepected behaviour under X11 if any
# terminal emulator running a login shell is closed.  
#
# For using "syslogout" with X11, better use this code from your
# X display manager's Xreset file as explained in README.usage.

if [ -f /etc/syslogout ]; then
    . /etc/syslogout
fi
