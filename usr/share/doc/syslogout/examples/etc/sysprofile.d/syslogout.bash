# Prevents syslogout logfiles from consuming too much space

# Uncomment for debugging:
#set -x

if [ -f /var/tmp/syslogout/$USER ]; then
     rm -f /var/tmp/syslogout/$USER
fi
