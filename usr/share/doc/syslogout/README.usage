Place your shell scripts containing modular configuration items into the
"/etc/syslogout.d/" directory.  Any files in "/etc/syslogout.d/" without
a .bash suffix will be ignored.

For shell sessions, the contents of "/etc/syslogout.d/" will be sourced
by every user at logout if the following lines are present in his
$HOME/.bash_logout:

    if [ -f /etc/syslogout ]; then
        . /etc/syslogout
    fi

If used for X sessions it is advisable to include the former statement
into the Xreset script of the X display manager instead to prevent that
closing of an terminal emulator window yields unexpected results in your
running X session if your X11 terminal emulator is using a login shell. 
Since Xreset is run by root, be sure then to run it under the user-id of
the X session's user:

---------------- to include in "/etc/X11/{w,x}dm/Xreset" -------------
# Run /etc/syslogout being $USER
su - $USER -c "if [ -f /etc/syslogout ]; then . /etc/syslogout; fi"
---------------- to include in "/etc/X11/{w,x}dm/Xreset" -------------

Users wanting to exclusively use own private configurations can simply
touch the file "$HOME/.nosyslogout" to stay practically unaffected by
the sysadmin's silly musings.  Alternatively users can set up their own
"$HOME/.syslogout.d/" directory containing private versions of the
*.bash files contained in "/etc/syslogout.d/" which will automagically
be preferred over the the system wide version.  To disable sourcing of a
single specific systemwide shell configuration file it is sufficient
create an empty version of it in "$HOME/.syslogout.d/".

If you need a similar mechanism for executing code at login time please
check out the related package "sysprofile" by yours truly, which is a
very close companion to "syslogout".

Package maintainers should please never ever rely on the existence of
"syslogout" on a system for their own Debian packages.  The "syslogout"
mechanism is and should stay an optional tool for local sysadmins only.

Feedback, additions and any improvements are happily appreciated by the
current package maintainer.
