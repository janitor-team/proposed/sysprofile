#!/bin/sh
#
# /etc/X11/Xsession
#
# global Xsession file -- used by both xdm and xinit (startx)

##########################################
##### Inserted for sysprofile usage ######
##### Don't use this sample file but #####
##### adapt your own /etc/X11/Xsession ###

if [ -f /etc/sysprofile ]; then
    . /etc/sysprofile
fi

##### Inserted for sysprofile usage ######
##########################################

optionfile=/etc/X11/Xsession.options

sysmodmap=/etc/X11/Xmodmap
usrmodmap=$HOME/.Xmodmap
sysresources=/etc/X11/Xresources
usrresources=$HOME/.Xresources

startup=$HOME/.xsession

errfile=$HOME/.xsession-errors

startssh=
sshagent=/usr/bin/ssh-agent

if grep -qs ^use-ssh-agent $optionfile; then
  if [ -x $sshagent -a -z "$SSH_AUTH_SOCK" ]; then
    startssh=yes
  fi
fi

if touch $errfile 2> /dev/null && [ -w $errfile ]; then
  chmod 600 "$errfile"
elif errfile=$(tempfile 2> /dev/null); then
  ln -sf "$errfile" "$TMPDIR/xsession-$USER"
else
  # fatal error
  echo "Xsession: unable to create X session error file.  Aborting."
  exit 1
fi
exec > "$errfile" 2>&1

case $# in
  0)
    ;;
  1)
    case "$1" in
      failsafe)
        if grep -qs ^allow-failsafe $optionfile; then
          if [ -e /usr/bin/x-terminal-emulator ]; then
            if [ -x /usr/bin/x-terminal-emulator ]; then
              exec x-terminal-emulator -geometry +1+1
            else
              # fatal error
              echo "Xsession: unable to launch failsafe X session; x-terminal-emulator not"
              echo " executable.  Aborting."
              exit 1
            fi
          else
            # fatal error
            echo "Xsession: unable to launch failsafe X session; x-terminal-emulator not found."
            echo " Aborting."
            exit 1
          fi
        fi
        ;;
      default)
        ;;
      *)
        program=$(which $1)
        if [ -e $program ]; then
          if [ -x $program ]; then
            startup=$program
          else
            echo "Xsession: unable to launch $1 X session; $1 not executable."
            echo "Xsession: Falling back to default session."
          fi
        else
          echo "Xsession: unable to launch $1 X session; $1 not found."
          echo "Xsession: Falling back to default session."
        fi
        ;;
    esac
    ;;
  *)
    echo "Xsession: unsupported number of arguments ($#)."
    echo "Xsession: Falling back to default session."
    ;;
esac

if [ -d $sysresources ]; then
  for resourcefile in $sysresources/*; do
    if [ -f $resourcefile ]; then
      if expr $resourcefile : '.*/[[:alnum:]_-]*$' > /dev/null 2>&1; then
        xrdb -merge $resourcefile
      fi
    fi
  done
fi

if [ -x /usr/bin/X11/xmodmap ]; then
  if [ -f $sysmodmap ]; then
    xmodmap $sysmodmap
  fi
fi

if grep -qs ^allow-user-resources $optionfile; then
  if [ -f $usrresources ]; then
    xrdb -merge $usrresources
  fi
fi

if [ -x /usr/bin/X11/xmodmap ]; then
  if grep -qs ^allow-user-modmap $optionfile; then
    if [ -f $usrmodmap ]; then
      xmodmap $usrmodmap
    fi
  fi
fi

if [ -e $startup ] && grep -qs ^allow-user-xsession $optionfile; then
  if [ -x $startup ]; then
    realstartup=$startup
  else
    realstartup="sh $startup"
  fi
elif [ -x /usr/bin/x-window-manager ]; then
  realstartup=x-window-manager
fi

if [ -z "$realstartup" ]; then
  if [ -x /usr/bin/x-terminal-emulator ]; then
    realstartup=x-terminal-emulator
  else
    # fatal error
    echo -n "Xsession: unable to start X session; "
    if grep -qs ^allow-user-xsession $optionfile; then
      echo -n "no $startup found, "
    fi
    echo "no window managers, and no terminal emulators found."
    echo " Aborting."
    exit 1
  fi
fi

if [ "$startssh" ]; then
  exec $sshagent $realstartup
else
  exec $realstartup
fi
