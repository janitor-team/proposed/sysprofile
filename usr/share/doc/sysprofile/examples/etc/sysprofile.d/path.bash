# This should rather be defined already in "/etc/profile".
# Use this only for adding to or overriding "$PATH". Note 
# that root needs different PATH settings than normal users.

#export PATH="$PATH:$HOME/bin"
