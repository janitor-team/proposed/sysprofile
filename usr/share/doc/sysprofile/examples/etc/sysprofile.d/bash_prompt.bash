# It's nice to know in which directory one is working 
# in without having to resort to /bin/pwd

#set -x

export PS1='[\u]\w > '
