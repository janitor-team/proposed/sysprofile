# XEmacs 21.4.4 takes a long nslookup time to find display.
# This here shortens it's startup time for local display.
#
# If xlock complains with "can not lock 127.0.0.1's display",
# use it adding the options "-remote -display 127.0.01:0.0".

export DISPLAY=127.0.0.1:0.0
