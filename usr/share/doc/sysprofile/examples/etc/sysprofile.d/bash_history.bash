# This defines the number of remembered commands and size of the history
# file.  Lazy people like large history file contents. ;-)

#set -x

# Defines the number of commands that will be remembered.

export HISTSIZE=5000
export HISTFILESIZE=5000

# The "ignoreboth" option ignores any double entry and makes sure that a
# command with a leading blank is not inluded in the history file at all.

export HISTCONTROL=ignoreboth
