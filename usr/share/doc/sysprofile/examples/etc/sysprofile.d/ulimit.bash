# The default ulimit.  See "man (1) builtins" for explanation.
# 
# Usage: ulimit [--SHacdflmnpstuv [limit]]
#
# Provides control over the resources available to the shell and to
# processes started by it.  The value of "limit" can be a number in the
# unit specified for the resource, or the value "unlimited".
#
# Just a few selected options:
#
#   -c   The maximum size of core files created
#   -f   The maximum size of files created by the shell
#   -l   The maximum size that may be locked into memory
#   -t   The maximum amount of cpu time in seconds
#   -u   The maximum number of processes available to a single user

# This is for those living on the edge - no limits! ;-)

#ulimit unlimited
