sysprofile for Debian
----------------------

The package "sysprofile" represents an approach for a modular centralized
configuration of the bash shell within a Debian Linux system.  It's main
component is the directory "/etc/sysprofile.d/"  which contains small
shell script modules executed at login.

Each of the files in "/etc/sysprofile.d/" contains a small specific
configuration setting item for either a single program or a specific
environment variable.  They are intended to split a monolithic
"/etc/profile" into small independent components for a more flexible
handling.  E.g., by just dropping in scripts via a network connection
without the need of having to start up an editor on every single remote
host one can easily distribute changed configuration settings to a whole
network.

While some assorted sample shell script modules are provided as
examples, the main accent lies on the implementation of a flexible
modular mechanism instead of a specific but limited configuration
setting which doesn't match anything but its author's own systems.

The sysprofile mechanism appears to be quite usable and has actually
been used successfully for more than a year.  Please examine and test it
closely before any serious use and share any criticism, corrections and
hopefully additions with all of us.  Especially nice would be the
addition of somewhat equivalent settings for other shells like (t)csh,
ksh or zsh, since this package primarily works for bash and has not been
tested otherwise.

If you need a similar mechanism for executing code at logout time please
check out the related package "syslogout" by yours truly, which is a
very close companion to "sysprofile".

Debian package maintainers please note:
---------------------------------------

Never *ever* rely on the existence of sysprofile on a system for your
own Debian packages.  The sysprofile mechanism is and should stay an
optional tool for local sysadmins only.

Massimo Dal Zotto <dz@cs.unitn.it> has developed a similar package
called "shellrc" which provides functionality not only for bash.  It
is hopefully still available from "http://www.cs.unitn.it/~dz/debian/".

If you happen to become inspired enough to make something really useful
out of this package i would be glad to hand over maintenance.  I just
tried to hack something i could use for my own purposes and because i
didn't find anything equally simple to use.

Paul Seelig <pseelig@debian.org>, Mon, 01 Oct 2001 21:13:54 +0200