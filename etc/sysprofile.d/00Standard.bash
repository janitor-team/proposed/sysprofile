# This here is for any settings you want.  If the contents
# become too abundant consider separating them into single 
# modules.  Please do read "/usr/share/doc/sysprofile/README.usage"
# for further information.  Sample modules can be found in the 
# samples subdirectory "/usr/share/doc/sysprofile/examples/".

# Might be handy for script debugging:

#if [ ! -d /var/tmp/sysprofile ]; then
#    mkdir -m 1777 /var/tmp/sysprofile
#    touch /var/tmp/sysprofile/$USER
#    chmod 600 /var/tmp/sysprofile/$USER
#else
#    touch /var/tmp/sysprofile/$USER
#    chmod 600 /var/tmp/sysprofile/$USER
#fi
