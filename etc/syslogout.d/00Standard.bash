# This here is for any settings you want.  If the contents
# become too abundant consider separating them into single 
# modules.  Please do read "/usr/share/doc/syslogout/README.usage"
# for further information.  Sample modules can be found in the 
# samples subdirectory "/usr/share/doc/syslogout/examples/".

# Might be handy for script debugging:

#if [ ! -d /var/tmp/syslogout ]; then
#    mkdir -m 1777 /var/tmp/syslogout
#    touch /var/tmp/syslogout/$USER
#    chmod 600 /var/tmp/syslogout/$USER
#else
#    touch /var/tmp/syslogout/$USER
#    chmod 600 /var/tmp/syslogout/$USER
#fi

